import random as rnd
import Dice as dice
from Combattant import Combattant

# Skill test results
SKILL_TEST_RES_NUMOF = 4
CRITICAL_FAIL = 0
MARGINAL_FAIL = 1
MARGINAL_SUCC = 2
CRITICAL_SUCC = 3

# Attack resolution results
ATTACK_RESOLUTION_RES_NUMOF = 10
BOTH_FUMBLE_4       = 0
ATTACKER_FUMBLE_3   = 1
ATTACKER_FUMBLE_4   = 2
ATTACKER_FUMBLE_5   = 3
DEFENDER_FUMBLE_3   = 4
BLOCK               = 5
STANDOFF_OR_MISS    = 6
ATTACKER_STRIKE_1   = 7
ATTACKER_STRIKE_2   = 8
ATTACKER_STRIKE_3   = 9

ATTACK_BLOCK_RESOLUTION = [
    [BOTH_FUMBLE_4, ATTACKER_FUMBLE_3, ATTACKER_FUMBLE_4, ATTACKER_FUMBLE_5],
    [DEFENDER_FUMBLE_3, BLOCK, STANDOFF_OR_MISS, ATTACKER_FUMBLE_3],
    [ATTACKER_STRIKE_2, ATTACKER_STRIKE_1, BLOCK, STANDOFF_OR_MISS],
    [ATTACKER_STRIKE_3, ATTACKER_STRIKE_2, ATTACKER_STRIKE_1, BLOCK]
]
PLAYERS_NUMOF = 4

def initiative_order(combs):
    rnd.shuffle(combs)
    return combs

def test_skill(comb):
    return rnd.randint(0, SKILL_TEST_RES_NUMOF-1)

def resolve_strike(att, defn, n):
    strike_location = dice.d100()
    impact_roll = dice.d6(n)
    strike_impact = impact_roll + att.weapon.main_impact


def resolve_attack(comb, oppos):
    defn = rnd.choice(oppos)
    print(" Defender %s" % defn) 
    att_res = test_skill(comb)
    print(" Attacker skill test result %d" % att_res)
    defn_res = test_skill(defn)
    print(" Defender skill test result %d" % defn_res)
    att_resolution = ATTACK_BLOCK_RESOLUTION[att_res][defn_res]
    if att_resolution == BOTH_FUMBLE_4:
        print(" Both fumble 4")
    elif att_resolution == ATTACKER_FUMBLE_3:
        print(" Attacker fumble 3")
    elif att_resolution == ATTACKER_FUMBLE_4:
        print(" Attacker fumble 4")
    elif att_resolution == ATTACKER_FUMBLE_5:
        print(" Attacker fumble 5")
    elif att_resolution == DEFENDER_FUMBLE_3:
        print(" Defender fumble 3")
    elif att_resolution == BLOCK:
        print(" Blocked")
    elif att_resolution == STANDOFF_OR_MISS:
        print(" Standoff/Miss")
    elif att_resolution == ATTACKER_STRIKE_1:
        print(" Attacker strike 1")
        resolve_strike(comb, defn, 1)
        print(" %s is dead" % defn.name)
        oppos.remove(defn)
    elif att_resolution == ATTACKER_STRIKE_2:
        print(" Attacker strike 2")
        print(" %s is dead" % defn.name)
        oppos.remove(defn)
    elif att_resolution == ATTACKER_STRIKE_3:
        print(" Attacker strike 3")
        print(" %s is dead" % defn.name)
        oppos.remove(defn)
    oppos.append(comb)
    return oppos

def combat_round(combs):
    initv_order = initiative_order(combs)
    print("Order of initiative:")
    for i in range(len(initv_order)):
        print(" %d: %s" % (i+1, initv_order[i]))
    orig_combs = combs.copy()
    for comb in orig_combs:
        if len(combs) > 1 and comb in combs:
            print("Attacker %s:" % comb)
            oppos = combs.copy()
            oppos.remove(comb)
            combs = resolve_attack(comb, oppos)
    return combs

def combat(combs):
    print("Combat for combattants: %s" % combs)
    round_ct = 0
    while len(combs) > 1:
        print("\nCombat round %d" % round_ct)
        combs = combat_round(combs)
        round_ct += 1
    print("The winner was %s" % combs[0])

def main():
    combattants = []
    combattants.append(Combattant("Ben the Brave"))
    combattants.append(Combattant("Alida the Awesome"))
    combattants.append(Combattant("Francesca the Fierce"))
    combattants.append(Combattant("Torbjorn the Terrible"))
    combattants.append(Combattant("Jude the Obscure"))
    combat(combattants)

if __name__ == "__main__":
    main()
