import random as rnd

HISTOGRAM_SAMPLES_NUMOF = 10000

def die(d, n):
    total = 0
    for _ in range(n):
        total += rnd.randint(1, d)
    return total

def d6(n=1):
    return(die(6, n))

def d100(n=1):
    return(die(6, n))

def hist(d, n):
    h = [0] * (d*n+2)
    for _ in range(HISTOGRAM_SAMPLES_NUMOF):
        x = die(d, n)
        h[x] += 1
    return h

def main():
    print("d6x2 roll distribution %s" % hist(6, 2))
    print("d6x3 roll distribution %s" % hist(6, 3))

if __name__ == "__main__":
    main()
