import Dice as dice
import Equipment as eqp

class Combattant:

    # Key Attributes
    KEY_ATTRIBUTES_NUMOF = 7
    INTELLIGENCE    = 0
    AURA            = 1
    WILL            = 2
    STRENGTH        = 3
    ENDURANCE       = 4
    DEXTERITY       = 5
    AGILITY         = 6
    KEY_ATTRIBUTES_ABRVS = ["int", "aur", "wil", "str", "end", "dex", "agi"]

    def __init__(self, name):
        self.name = name
        self.key_attributes = [0] * self.KEY_ATTRIBUTES_NUMOF 
        for i in range(self.KEY_ATTRIBUTES_NUMOF):
            self.key_attributes[i] = dice.d6(3)
        self.weapon = eqp.Weapon()

    def __str__(self):
        s = f"{self.name} - "
        for i in range(self.KEY_ATTRIBUTES_NUMOF):
            s += f"{self.KEY_ATTRIBUTES_ABRVS[i]}: {self.key_attributes[i]}"
            if i < self.KEY_ATTRIBUTES_NUMOF - 1:
                s+= ", "
        s += f"\n {self.weapon}"
        return s

def main():
    c = Combattant("Barry the Quick")
    print(c)

if __name__ == "__main__":
    main()
