import random as rnd

# Weapons
WEAPON_TYPES_NUMOF   = 3
DAGGER      = 0
CLUB        = 1
HANDAXE     = 2
BROADSWORD  = 3
WEAPON_NAMES = ["dagger", "club", "handaxe", "broadsword"]

WEAPON_IMPACT_ASPECTS_NUMOF = 3
BLUNT   = 0
EDGE    = 1
POINT   = 2
ASPECT_NAMES = ["blunt", "edge", "point"]
ASPECT_ACTION_NAMES = ["whacks", "strikes", "stabs"]

WEAPON_TYPE_IMPACTS = [
    [1, 2,  4],
    [3, -1, -1],
    [4, 6,  4],
    [3, 5,  3]
]

class Weapon:

    def __init__(self, name=""):
        if name == "":
            self.name = rnd.choice(WEAPON_NAMES)
        elif not name in WEAPON_NAMES:
            raise Exception("Creating weapon with unknown weapon name")
        else:
            self.name = name
        widx = WEAPON_NAMES.index(self.name)
        self.impacts = WEAPON_TYPE_IMPACTS[widx]
        self.main_aspect = self.impacts.index(max(self.impacts))
        self.main_impact = self.impacts[self.main_aspect]

    def __str__(self):
        s = f"{self.name} - "
        s += f"impact: {self.impacts[self.main_aspect]}, "
        s += f"aspect: {ASPECT_NAMES[self.main_aspect]}"
        return s
